import { battleship, ship } from "./battleship"
import * as d3 from "d3"

export {
  bs as bs,
  bs2 as bs2,
  render as render
}

const cellSize = 30
const smallCell = 20
const bs = new battleship()
const bs2 = new battleship()
const selBoard = new battleship(5,5)
const board1 = d3.select("#attackBoard").append("g")
const board2 = d3.select("#defendBoard").append("g")
let shipIndex = 0
let gameStart = false


function render() {
  if (gameStart) 
    renderAttackBoard()
  else 
    renderButtons()
  
  renderDefendBoard()
}

function renderAttackBoard() {
  
  board1.selectAll("*").remove();
  let attack = board1.selectAll("rect").data(bs.board)

  attack.attr("class", (d) => {
    
    switch(d) { 
      case 0: { 
         return "cell"
         break; 
      } 
      case 1: { 
         return "cell hit" 
         break; 
      } 
      case 2: {
         return "cell miss"
      }
      default: { 
         return "cell" 
         break; 
      } 
    }            
  })

  attack.enter()
    .append("rect")
    .attr("class", (d) => {
      switch(d) { 
        case 0: { 
          return "cell"
          break; 
        } 
        case 1: { 
          return "cell hit" 
          break; 
        } 
        case 2: {
          return "cell miss"
          break;
        }
        default: { 
          return "cell" 
          break; 
        } 
      }          
    })
    .attr("x", (val,i) => {
      return bs.x(i) * cellSize
    })
    .attr("y", (val,i) => {
      return bs.y(i) * cellSize
    })
    .attr("width", cellSize)
    .attr("height", cellSize)
    .on("click", (d,i) => {
      let x = bs.x(i)
      let y = bs.y(i)
      if(fire(bs2,x,y)) {
        bs.setHit(x,y)
      }
      else {
        bs.setMiss(x,y)
      }
      renderAttackBoard()
    })
}

function renderDefendBoard() {

  let defend = gameStart ? board2.selectAll("rect").data(bs2.board) : board1.selectAll("rect").data(bs2.board)
  
  defend.attr("class", (d) => {
    return d == 2 ? "cell ship" : "cell"            
  })

  defend.enter()
    .append("rect")
    .attr("class", (d) => {
      return d == 2 ? "cell ship" : "cell"
    })
    .attr("x", (val,i) => {
      return bs2.x(i) * cellSize
    })
    .attr("y", (val,i) => {
      return bs2.y(i) * cellSize
    })
    .attr("width", cellSize)
    .attr("height", cellSize)
    .on("click", (d,i) => {
      let x = bs2.x(i)
      let y = bs2.y(i)
      if(!bs2.ships[shipIndex].placed) {
        bs2.setShip(x, y, bs2.ships[shipIndex])
        bs2.ships[shipIndex].placed = true
      }
      renderDefendBoard()
    })
}

function renderButtons() {

  document.getElementById("destroyer")!.addEventListener("click", () => {
    shipIndex = 0
  })

  document.getElementById("cruiser")!.addEventListener("click", () => {
    shipIndex = 1
  })

  document.getElementById("submarine")!.addEventListener("click", () => {
    shipIndex = 2
  })

  document.getElementById("battleship")!.addEventListener("click", () => {
    shipIndex = 3
  })

  document.getElementById("carrier")!.addEventListener("click", () => {
    shipIndex = 4
  })

  document.getElementById("rotate")!.addEventListener("click", () => {
    bs2.ships[shipIndex].rotation = bs2.ships[shipIndex].rotation ? 0 : 1
  })

  document.getElementById("begin")!.addEventListener("click", () => {
    gameStart = true
    let butDiv = document.getElementById("but")!
    butDiv.hidden = true
    render()
  })
}

function fire(b:battleship, x:number, y:number) {
    let index = b.boardIndex(x, y)
    return b.board[index] == 2
}


