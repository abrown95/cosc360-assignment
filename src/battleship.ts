
export {
    battleship as battleship,
    ship as ship
  }


class battleship {

    constructor(public w:number = 10, public h:number = 10) {
        this.board = this.emptyBoard()
    }

    board: Array<number> 
    destroyer = new ship(2, 0, "Destroyer")
    cruiser = new ship(3, 0, "Cruiser")
    submarine = new ship(3, 0, "Submarine")
    battle = new ship(4, 0, "Battleship")
    carrier = new ship(5, 0, "Carrier")

    ships = [this.destroyer, this.cruiser, this.submarine, this.battle, this.carrier]
    
    emptyBoard():Array<number> {
        let arr = <number[]>[]
        for (let i = 0; i < this.h * this.w; i++) {
            arr.push(0)
        }
        return arr
    }

    boardIndex(x:number, y:number):number {
        return y * this.w + x
    }

    x(index:number) {
        return index % this.w
    }

    y(index:number) {
        return Math.floor(index / this.w)
    }

    setHit(x:number, y:number):void {
        let b = this.boardIndex(x,y)
        this.board[b] = 1
    }

    setMiss(x:number, y:number):void {
        let b = this.boardIndex(x,y)
        this.board[b] = 2
    }

    setShip(x:number, y:number, ship:ship):void {
        let b = this.boardIndex(x,y)
        if (this.isValidPlacement(x,y,ship.size,ship.rotation) == 1) {
            if (ship.rotation == 0) {
                for (let i = 0; i < ship.size; i++){
                    this.board[b+i] = 2
                }
            }
            else {
                for (let i = 0; i < ship.size; i++) {
                    let by = this.boardIndex(x, y + i)
                    this.board[by] = 2
                }    
            }
        }
    }

    isValidPlacement(x:number, y:number, ship:number, rotation:number):number {
        let b = this.boardIndex(x,y)
        let flag = 0
        if (!rotation) {
            
            if (this.w - x >= ship) {
                flag = 1
            }

            for (let i = 0; i < ship; i++) {
                if (this.board[b+i] == 2) {
                    flag = 0
                }
            }
        }
        else {
            console.log("insideb")
            if (this.h - y >= ship) {
                flag = 1
            }
            for (let i = 0; i < ship; i++) {
                let by = this.boardIndex(x, y + i)
                if (by < 100 && this.board[by] == 2) {
                    flag = 0
                }
            }
        }
        
        return flag
    }
}

class ship {
    size:number
    rotation:number
    name:string
    placed:boolean

    constructor(s:number, r:number, n:string) {
        this.size = s
        this.rotation = r
        this.name = n
        this.placed = false
    }
}