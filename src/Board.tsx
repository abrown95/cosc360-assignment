import { battleship, ship } from "./battleship"
import * as React from "react"
import * as ReactDOM from "react-dom"


export class Board extends React.Component {
    cellSize = 30
    bs = new battleship()
    createGrid = () => {
        let grid = []

        for (let i = 0; i < this.bs.w; i++) {
            let row = []

            for (let j = 0; j < this.bs.h; j++) {
                row.push(<rect
                        className="cell"
                        x={+(j * this.cellSize)}
                        y={+(i * this.cellSize)}
                        width={+(this.cellSize)}
                        height={+(this.cellSize)}
                            />)
            }
            grid.push(row)
        }
        return grid
    }

    render() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="300" height="300">
                <g>
                    {this.createGrid()}
                </g>
            </svg>
        );
    }
}

