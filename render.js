"use strict";
/// <reference types="d3" />
var cellSize = 30;
var bs = new battleship();
var game = d3.select("#attackBoard").append("g");
function render() {
    console.log("rendering");
    var update = game.selectAll("rect").data(bs.board);
    update.attr("class", function (d) {
        return d == 1 ? "cell hit" : "cell";
    });
    update.enter()
        .append("rect")
        .attr("class", function (d) {
        return d == 1 ? "cell hit" : "cell";
    })
        .attr("x", function (val, i) {
        return bs.x(i) * cellSize;
    })
        .attr("y", function (val, i) {
        return bs.y(i) * cellSize;
    })
        .attr("width", cellSize)
        .attr("height", cellSize)
        .on("click", function (d, i) {
        var x = bs.x(i);
        var y = bs.y(i);
        bs.toggleCell(x, y);
        render();
    });
}
