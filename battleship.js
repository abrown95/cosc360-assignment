"use strict";
var battleship = /** @class */ (function () {
    function battleship(w, h) {
        if (w === void 0) { w = 10; }
        if (h === void 0) { h = 10; }
        this.w = w;
        this.h = h;
        this.board = this.emptyBoard();
    }
    battleship.prototype.emptyBoard = function () {
        var arr = [];
        for (var i = 0; i < this.h * this.w; i++) {
            arr.push(0);
        }
        return arr;
    };
    battleship.prototype.boardIndex = function (x, y) {
        return y * this.w + x;
    };
    battleship.prototype.x = function (index) {
        return index % this.w;
    };
    battleship.prototype.y = function (index) {
        return Math.floor(index / this.w);
    };
    battleship.prototype.toggleCell = function (x, y) {
        var b = this.boardIndex(x, y);
        //this.board[b] = !this.board[b]
    };
    battleship.prototype.isAlive = function (x, y) {
        var b = this.boardIndex(x, y);
        return (x >= 0) && (y >= 0) && (x < this.w) && (y < this.h)
            && this.board[b] == 1;
    };
    return battleship;
}());
